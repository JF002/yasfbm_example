#include <cstdint>
extern "C" {
#include <std_drv/inc/bl602_common.h>
#include <std_drv/inc/bl602_glb.h>
#include <std_drv/inc/bl602_hbn.h>
}

int main() {
  GLB_GPIO_Cfg_Type gpio_cfg;

  gpio_cfg.gpioFun = GPIO_FUN_GPIO;
  gpio_cfg.gpioPin = 11;
  gpio_cfg.drive = 0;
  gpio_cfg.smtCtrl = 1;
  gpio_cfg.gpioMode = GPIO_MODE_OUTPUT;
  gpio_cfg.pullType = GPIO_PULL_UP;

  GLB_GPIO_Init(&gpio_cfg);

  gpio_cfg.gpioPin = 17;
  GLB_GPIO_Init(&gpio_cfg);

  gpio_cfg.gpioPin = 14;
  GLB_GPIO_Init(&gpio_cfg);

  GLB_GPIO_Write(static_cast<GLB_GPIO_Type>(11), 1);
  GLB_GPIO_Write(static_cast<GLB_GPIO_Type>(17), 1);
  GLB_GPIO_Write(static_cast<GLB_GPIO_Type>(14), 1);

  while (1) {
    GLB_GPIO_Write(static_cast<GLB_GPIO_Type>(11), 0);
    BL602_Delay_MS(300);
    GLB_GPIO_Write(static_cast<GLB_GPIO_Type>(11), 1);
    BL602_Delay_MS(300);

    GLB_GPIO_Write(static_cast<GLB_GPIO_Type>(17), 0);
    BL602_Delay_MS(300);
    GLB_GPIO_Write(static_cast<GLB_GPIO_Type>(17), 1);
    BL602_Delay_MS(300);

    GLB_GPIO_Write(static_cast<GLB_GPIO_Type>(14), 0);
    BL602_Delay_MS(300);
    GLB_GPIO_Write(static_cast<GLB_GPIO_Type>(14), 1);
    BL602_Delay_MS(300);
  }
  return 0;
}
