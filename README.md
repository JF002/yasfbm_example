# Example application for YASFBM

This is an example application for [YASFBM](https://codeberg.org/JF002/yasfbm).

This application blinks the 3 LEDs installed on the [PineCone from Pine64](https://wiki.pine64.org/wiki/PineCone).

![video](doc/yasfbm_example.gif)